package com.neutech.enumeration;

public enum CategoryStatus implements StatusBehaviour {
    IN_USE(1, "正常"),
    UN_USE(2, "已废弃");

    private final Integer statusCode;
    private final String statusMsg;

    CategoryStatus(Integer statusCode, String statusMsg) {
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    @Override
    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String getStatusMsg() {
        return statusMsg;
    }
}
