package com.neutech.enumeration;

public interface StatusBehaviour {

    Integer getStatusCode();

    String getStatusMsg();
}
