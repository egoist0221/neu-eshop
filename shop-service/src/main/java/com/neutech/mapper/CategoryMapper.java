package com.neutech.mapper;

import com.neutech.entity.Category;
import com.neutech.vo.CategoryVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {

    CategoryVO selectVOById(Integer id);

    List<CategoryVO> selectByParentId(Integer parentId);

    List<CategoryVO> selectAllVO();

    List<CategoryVO> selectAllRootVO();

    String selectNameByCategoryId(Integer id);
}
