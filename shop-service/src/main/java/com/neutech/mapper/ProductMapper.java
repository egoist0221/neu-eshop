package com.neutech.mapper;

import com.neutech.arg.ProductSelectArgs;
import com.neutech.vo.ProductInfoVO;
import com.neutech.vo.ProductVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {

    ProductVO selectVOById(Integer id);

    ProductInfoVO selectInfoById(Integer id);

    List<ProductInfoVO> selectAllVOByArgs(ProductSelectArgs args);
}
