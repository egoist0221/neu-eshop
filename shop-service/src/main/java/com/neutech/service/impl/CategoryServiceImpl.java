package com.neutech.service.impl;

import com.neutech.enumeration.ResultExceptionEnum;
import com.neutech.mapper.CategoryMapper;
import com.neutech.service.CategoryService;
import com.neutech.vo.CategoryVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "categoryCache")
public class CategoryServiceImpl implements CategoryService {

    private final CategoryMapper mapper;

    @Autowired
    public CategoryServiceImpl(CategoryMapper categoryMapper) {
        this.mapper = categoryMapper;
    }

    @Override
    public ResultVO<List<CategoryVO>> getCategorys() {
        List<CategoryVO> catagoryVOList = mapper.selectAllVO();
        if (catagoryVOList == null) {
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXIST);
        }
        return ResultVO.success(catagoryVOList);
    }

    @Override
    public ResultVO<List<CategoryVO>> getRootCategorys() {
        List<CategoryVO> catagoryVOList = mapper.selectAllRootVO();
        if (catagoryVOList == null) {
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXIST);
        }
        return ResultVO.success(catagoryVOList);
    }

    @Override
    @Cacheable(key = "#categoryId")
    public String getCategoryName(int categoryId) {
        return mapper.selectNameByCategoryId(categoryId);
    }
}
