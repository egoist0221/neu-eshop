package com.neutech.service;

import com.neutech.entity.CartItem;
import com.neutech.vo.CartItemVO;
import com.neutech.vo.ResultVO;

import java.util.List;

public interface CartService {
    ResultVO<CartItemVO> addItem(int userId, int productId, int quantity);

    ResultVO<Void> deleteItem(int userId, int productId);

    ResultVO<Void> deleteItems(int userId, List<Integer> productIdList);

    ResultVO<List<CartItemVO>> selectAllItem(int userId);

    ResultVO<CartItemVO> updateItem(int userId, int productId, int quantity, int checked);
}
