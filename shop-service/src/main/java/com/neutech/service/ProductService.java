package com.neutech.service;

import com.neutech.arg.ProductSelectArgs;
import com.neutech.vo.ProductInfoVO;
import com.neutech.vo.ProductVO;
import com.neutech.vo.ResultVO;

import java.util.List;

public interface ProductService {
    ResultVO<ProductVO> selectProductById(Integer productId);

    ResultVO<ProductInfoVO> selectProductInfoById(Integer productId);

    ResultVO<List<ProductInfoVO>> selectPagedProductByArg(Integer page, int pageSize, ProductSelectArgs args);
}
