package com.neutech.service;

import com.neutech.vo.CategoryVO;
import com.neutech.vo.ResultVO;

import java.util.List;

public interface CategoryService {

    ResultVO<List<CategoryVO>> getCategorys();

    ResultVO<List<CategoryVO>> getRootCategorys();

    String getCategoryName(int categoryId);
}
