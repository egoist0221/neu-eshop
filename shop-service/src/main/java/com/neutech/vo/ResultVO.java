package com.neutech.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.neutech.enumeration.ResultExceptionEnum;

import java.io.Serializable;

public class ResultVO<T> implements Serializable {

    private Integer code;
    private String msg;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    private static final Integer DEFAULT_SUCCESS_CODE = 0;
    private static final String DEFAULT_SUCCESS_MSG = "成功";

    public static <T> ResultVO<T> success() {
        return new ResultVO<>(DEFAULT_SUCCESS_CODE, DEFAULT_SUCCESS_MSG);
    }

    public static <T> ResultVO<T> success(T data) {
        ResultVO<T> resultVO = new ResultVO<>();
        resultVO.code = DEFAULT_SUCCESS_CODE;
        resultVO.msg = DEFAULT_SUCCESS_MSG;
        resultVO.data = data;
        return resultVO;
    }

    public static <T> ResultVO<T> error(Integer code, String msg) {
        ResultVO<T> resultVO = new ResultVO<>();
        resultVO.code = code;
        resultVO.msg = msg;
        return resultVO;
    }

    public static <T> ResultVO<T> error(ResultExceptionEnum rEnum) {
        return error(rEnum.getCode(), rEnum.getMessage());
    }

    public ResultVO() {
    }

    public ResultVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ResultVO{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
