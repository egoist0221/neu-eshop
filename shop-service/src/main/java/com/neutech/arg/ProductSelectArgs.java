package com.neutech.arg;

import java.math.BigDecimal;

public class ProductSelectArgs {
    private Integer categoryId;
    private String name;
    private BigDecimal miniumPrice;
    private BigDecimal maxiumPrice;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.charAt(0) != '%')
            name = '%' + name;
        if (name.charAt(name.length() - 1) != '%')
            name += '%';
        this.name = name;
    }

    public BigDecimal getMiniumPrice() {
        return miniumPrice;
    }

    public void setMiniumPrice(BigDecimal miniumPrice) {
        this.miniumPrice = miniumPrice;
    }

    public BigDecimal getMaxiumPrice() {
        return maxiumPrice;
    }

    public void setMaxiumPrice(BigDecimal maxiumPrice) {
        this.maxiumPrice = maxiumPrice;
    }
}
