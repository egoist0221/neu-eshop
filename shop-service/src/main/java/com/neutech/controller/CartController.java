package com.neutech.controller;

import com.neutech.form.CartForm;
import com.neutech.service.CartService;
import com.neutech.vo.CartItemVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CartController {

    private final CartService service;

    @Autowired
    public CartController(CartService service) {
        this.service = service;
    }

    @GetMapping("/cart")
    public ResultVO<List<CartItemVO>> getCart(@RequestParam("userId") int userId) {
        return service.selectAllItem(userId);
    }

    @PostMapping("/cart/add")
    public ResultVO<CartItemVO> addItem(@RequestParam("userId") int userId,
                                        @RequestParam("productId") int productId,
                                        @RequestParam(value = "quantity", defaultValue = "1") int quantity) {
        return service.addItem(userId, productId, quantity);
    }

    @PostMapping("/cart/delete")
    public ResultVO<Void> deleteItem(@RequestParam("userId") int userId,
                                     @RequestParam("productId") int productId) {
        return service.deleteItem(userId, productId);
    }

    @PostMapping("/cart/multiDelete")
    public ResultVO<Void> deleteItems(@RequestBody CartForm cartForm) {
        return service.deleteItems(cartForm.getUserId(), cartForm.getProductIdList());
    }

    @PostMapping("/cart/update")
    public ResultVO<CartItemVO> changeItem(@RequestParam("userId") int userId,
                                           @RequestParam("productId") int productId,
                                           @RequestParam(value = "quantity") int quantity,
                                           @RequestParam(value = "checked") int checked) {
        return service.updateItem(userId, productId, quantity, checked);
    }
}
