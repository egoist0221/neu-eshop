package com.neutech.controller;

import com.neutech.arg.ProductSelectArgs;
import com.neutech.service.ProductService;
import com.neutech.vo.ProductInfoVO;
import com.neutech.vo.ProductVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    private final ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping("/product")
    public ResultVO<ProductVO> getProductById(@RequestParam("id") Integer id) {
        return service.selectProductById(id);
    }


    @GetMapping("/products")
    public ResultVO<List<ProductInfoVO>> getProducts(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                     @RequestParam(value = "categoryId", required = false) Integer categoryId){
        ProductSelectArgs productSelectArgs = new ProductSelectArgs();
        productSelectArgs.setCategoryId(categoryId);
        return service.selectPagedProductByArg(page, 12, productSelectArgs);
    }

}
