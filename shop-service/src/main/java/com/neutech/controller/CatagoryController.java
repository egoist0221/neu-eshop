package com.neutech.controller;

import com.neutech.service.CategoryService;
import com.neutech.service.ProductService;
import com.neutech.vo.CategoryVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CatagoryController {

    private final CategoryService service;

    @Autowired
    public CatagoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/cateGorys")
    public ResultVO<List<CategoryVO>> getCateGorys() {
        return service.getCategorys();
    }

    @GetMapping("/rootCateGory")
    public ResultVO<List<CategoryVO>> getRootCateGorys() {
        return service.getRootCategorys();
    }
}
