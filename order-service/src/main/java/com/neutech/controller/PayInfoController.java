package com.neutech.controller;

import com.neutech.entity.PayInfo;
import com.neutech.service.PayInfoService;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PayInfoController {

    private final PayInfoService service;

    @Autowired
    public PayInfoController(PayInfoService service) {
        this.service = service;
    }

    @GetMapping("/payInfo")
    public ResultVO<PayInfo> payOrder(@RequestParam("orderNo") Long orderNo) {
        return service.getPayInfoByOrderNo(orderNo);
    }

    @PostMapping("/pay")
    public ResultVO<Void> payOrder(
            @RequestParam("userId") Integer userId,
            @RequestParam("orderNo") Long orderNo,
            @RequestParam("payPlatform") Integer payPlatform) {
        return service.openPay(userId, orderNo, payPlatform);
    }

    @PostMapping("/finishPay")
    public ResultVO<Void> finishPay(@RequestParam("orderNo") Long orderNo) {
        return service.finishPay(orderNo);
    }
}
