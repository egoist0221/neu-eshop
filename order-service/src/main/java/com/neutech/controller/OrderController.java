package com.neutech.controller;

import com.neutech.form.OrderForm;
import com.neutech.service.OrderService;
import com.neutech.vo.OrderVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class OrderController {
    private final OrderService service;

    @Autowired
    public OrderController(OrderService service) {
        this.service = service;
    }

    @GetMapping("/order")
    public ResultVO<OrderVO> getOrdersByUserID(@RequestParam("orderNo") Long orderNo) {
        return service.getOrderByOrderNo(orderNo);
    }

    @GetMapping("/orders")
    public ResultVO<List<OrderVO>> getOrdersByUserID(@RequestParam("userId") Integer userId,
                                                     @RequestParam(value = "orderStatus", required = false) Byte orderStatus) {
        return service.getOrderByUserIdAndStatus(userId, orderStatus);
    }

    @PostMapping("/createOrder")
    public ResultVO<OrderVO> createOrder(@RequestBody OrderForm orderForm) {
        return service.orderCreate(orderForm);
    }
}
