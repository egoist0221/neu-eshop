package com.neutech.service;

import com.neutech.entity.Product;
import com.neutech.vo.ResultVO;

public interface ProductService {
    ResultVO<Product> getProduct(Integer productId);
}
