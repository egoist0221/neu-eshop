package com.neutech.service;

import com.neutech.entity.OrderItem;
import com.neutech.vo.ResultVO;

import java.util.List;

public interface OrderItemService {
    ResultVO<List<OrderItem>> getOrderItemByOrderNo(Long orderNo);

    ResultVO<Void> addOrderItem(Integer userId, Long orderNo, Integer productId, Integer quantity);

    ResultVO<Void> addOrderItems(List<OrderItem> itemList);
}
