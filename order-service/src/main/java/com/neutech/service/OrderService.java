package com.neutech.service;

import com.neutech.form.OrderForm;
import com.neutech.vo.OrderVO;
import com.neutech.vo.ResultVO;

import java.util.List;

public interface OrderService {
    ResultVO<OrderVO> getOrderByOrderNo(Long orderNo);

    ResultVO<List<OrderVO>> getOrderByUserIdAndStatus(Integer userId, Byte orderStatus);

    ResultVO<OrderVO> orderCreate(OrderForm orderForm);

    ResultVO<Void> orderOpenPay(Long orderNo);

    ResultVO<Void> orderFinishPay(Long orderNo);

    ResultVO<Void> orderClosePay(Long orderNo);
}
