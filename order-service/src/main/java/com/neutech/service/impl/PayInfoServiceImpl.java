package com.neutech.service.impl;

import com.neutech.entity.PayInfo;
import com.neutech.enumeration.OrderStatusEnum;
import com.neutech.enumeration.ResultExceptionEnum;
import com.neutech.mapper.PayInfoMapper;
import com.neutech.service.OrderService;
import com.neutech.service.PayInfoService;
import com.neutech.vo.OrderVO;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PayInfoServiceImpl implements PayInfoService {
    private final PayInfoMapper mapper;
    private final OrderService orderService;

    @Autowired
    public PayInfoServiceImpl(PayInfoMapper mapper, OrderService orderService) {
        this.mapper = mapper;
        this.orderService = orderService;
    }

    @Override
    public ResultVO<PayInfo> getPayInfoByOrderNo(Long orderNo) {
        PayInfo info = mapper.selectByOrderNo(orderNo);
        if (info == null)
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);
        else
            return ResultVO.success(info);
    }

    @Override
    public ResultVO<Void> openPay(Integer userId, Long orderNo, Integer payPlatform) {
        ResultVO<OrderVO> orderResult = orderService.getOrderByOrderNo(orderNo);
        if (orderResult.getCode() != 0)
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);
        OrderVO orderVO = orderResult.getData();

        PayInfo payInfo = new PayInfo();
        payInfo.setUserId(userId);
        payInfo.setOrderNo(orderNo);
        payInfo.setPayPlatform(payPlatform);
        //模拟支付
        payInfo.setPlatformNumber("14890abc" + orderNo);
        payInfo.setPlatformStatus("未支付");
        payInfo.setCreateTime(new Date());

        if (mapper.insert(payInfo) != 1)
            return ResultVO.error(ResultExceptionEnum.UNKNOWN_ERROR_OCCURED);


        ResultVO<Void> changeOrderStatusResult = orderService.orderOpenPay(orderNo);
        if (changeOrderStatusResult.isFail())
            return changeOrderStatusResult;

        return ResultVO.success();
    }

    @Override
    public ResultVO<Void> finishPay(Long orderNo) {
        PayInfo payInfo = mapper.selectByOrderNo(orderNo);
        if (payInfo == null)
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);

        PayInfo newInfo = new PayInfo();
        newInfo.setId(payInfo.getId());
        newInfo.setPlatformStatus("已支付");
        newInfo.setUpdateTime(new Date());

        switch (mapper.updateByPrimaryKeySelective(newInfo)) {
            case 0:
                return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);

            case 1:
                ResultVO<Void> changeOrderStatusResult = orderService.orderFinishPay(orderNo);
                if (changeOrderStatusResult.isFail())
                    return changeOrderStatusResult;

                return ResultVO.success();

            default:
                return ResultVO.error(ResultExceptionEnum.UNKNOWN_ERROR_OCCURED);
        }
    }
}
