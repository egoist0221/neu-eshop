package com.neutech.service.impl;

import com.neutech.entity.Product;
import com.neutech.enumeration.ResultExceptionEnum;
import com.neutech.mapper.ProductMapper;
import com.neutech.service.ProductService;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductMapper mapper;

    @Autowired
    public ProductServiceImpl(ProductMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ResultVO<Product> getProduct(Integer productId) {
        Product product = mapper.selectForOrderItemById(productId);
        if (product == null) {
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);
        }
        return ResultVO.success(product);
    }
}
