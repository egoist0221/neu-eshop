package com.neutech.service;

import com.neutech.form.CartForm;
import com.neutech.vo.ResultVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient("shop-service")
public interface CartService {
    @PostMapping("/shop/multiDelete")
    ResultVO<Void> orderItemRemove(@RequestBody CartForm cartForm);
}
