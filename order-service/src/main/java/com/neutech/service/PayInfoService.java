package com.neutech.service;

import com.neutech.entity.PayInfo;
import com.neutech.vo.ResultVO;

public interface PayInfoService {
    ResultVO<PayInfo> getPayInfoByOrderNo(Long orderNo);

    ResultVO<Void> openPay(Integer userId, Long orderNo, Integer payPlatform);

    ResultVO<Void> finishPay(Long orderNo);
}
