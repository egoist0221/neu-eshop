package com.neutech.vo;

import com.neutech.entity.OrderItem;
import com.neutech.enumeration.OrderStatusEnum;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OrderVO {
    private Long orderNo;
    private Integer userId;
    private BigDecimal payment;
    private Byte paymentType;
    private OrderStatusEnum orderStatus;
    private List<OrderItem> items;
    private String createDate;
    private String updateDate;
    private Integer postage;

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public Byte getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Byte paymentType) {
        this.paymentType = paymentType;
    }

    public OrderStatusEnum getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatusEnum orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public void setPostage(Integer postage) {
        this.postage = postage;
    }

    public Integer getPostage() {
        return postage;
    }
}
