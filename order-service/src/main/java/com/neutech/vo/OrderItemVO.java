package com.neutech.vo;

import java.math.BigDecimal;
import java.util.Date;

public class OrderItemVO {
    private Integer id;
    private Integer userId;
    private Long orderNo;
    private Integer productId;
    private String productName;
    private String productImage;
    private BigDecimal currentUnitPrice;
    private Integer quantity;
    private BigDecimal totalPrice;
}
