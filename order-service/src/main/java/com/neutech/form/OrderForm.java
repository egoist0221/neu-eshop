package com.neutech.form;

import com.neutech.pojo.OrderItemPOJO;

import javax.validation.constraints.NotNull;
import java.util.List;

public class OrderForm {
    @NotNull(message = "未知用户")
    private int userId;

    @NotNull(message = "不允许生成空订单")
    private List<OrderItemPOJO> itemList;

    @NotNull(message = "请指定收货地址")
    private Integer shippingId;

    @NotNull(message = "请指定付款方式")
    private byte paymentType;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<OrderItemPOJO> getItemList() {
        return itemList;
    }

    public void setItemList(List<OrderItemPOJO> itemList) {
        this.itemList = itemList;
    }

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    public Byte getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Byte paymentType) {
        this.paymentType = paymentType;
    }
}
