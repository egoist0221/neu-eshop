package com.neutech.pojo;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class OrderItemPOJO {
    @NotNull(message = "1")
    private int productId;
    @NotNull(message = "2")
    private int quantity;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
