package com.neutech.enumeration;

public enum ResultExceptionEnum {
    FORMAT_EXCEPTION(1001, "数据格式异常"),
    DATA_NON_EXISTS(1002, "数据不存在"),
    ILLEGAL_ACTION(1003, "非法访问"),

    USERNAME_IN_USE(2001, "用户名已被使用"),
    LOGIN_NOT_MATCH(2002, "账号与密码不匹配"),

    PRODUCT_NOT_EXIST(3002, "商品不存在"),

    UNKNOWN_ERROR_OCCURED(9999, "发生未知错误");

    private final Integer code;
    private final String message;

    ResultExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
