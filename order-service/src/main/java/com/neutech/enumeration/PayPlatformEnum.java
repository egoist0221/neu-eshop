package com.neutech.enumeration;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PayPlatformEnum {
    ON_SALES(101, "支付宝"),
    OFF_THE_SHELF(102, "微信");

    private final Integer statusCode;
    private final String statusMsg;

    PayPlatformEnum(Integer statusCode, String statusMsg) {
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public static PayPlatformEnum getPayPlatformEnum(Integer code) {
        for (PayPlatformEnum value : values()) {
            if (value.getStatusCode().equals(code)) {
                return value;
            }
        }
        return null;
    }

}
