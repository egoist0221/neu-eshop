package com.neutech.enumeration;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OrderStatusEnum {
    ORDER_CANCELLED((byte) 0, "已取消"),
    ORDER_UNPAID((byte) 1, "未付款"),
    ORDER_PAID((byte) 2, "已付款"),
    PRODUCT_SHIPPED((byte) 3, "已发货"),
    TRANSACTION_DONE((byte) 4, "交易成功"),
    TRANSACTION_CLOSED((byte) 5, "交易关闭");

    private final Byte statusCode;
    private final String statusMsg;

    OrderStatusEnum(Byte statusCode, String statusMsg) {
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    public Byte getStatusCode() {
        return statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public static OrderStatusEnum getStatus(Byte code) {
        for (OrderStatusEnum value : values()) {
            if (value.getStatusCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}
