package com.neutech.mapper;

import com.neutech.entity.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {
    Product selectForOrderItemById(Integer ProductId);
}
