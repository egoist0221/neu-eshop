package com.neutech.mapper;

import com.neutech.entity.Order;
import com.neutech.enumeration.OrderStatusEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    Order selectByOrderNo(Long orderNo);

    List<Order> selectAllByUserIdSelective(@Param("userId") Integer userId,
                                           @Param("orderStatus") Byte orderStatus);

    int updateByOrderNoSelective(Order order);
}