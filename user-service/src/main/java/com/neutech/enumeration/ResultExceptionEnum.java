package com.neutech.enumeration;

public enum ResultExceptionEnum {
    FORMAT_EXCEPTION(1001, "数据格式异常"),
    DATA_NON_EXISTS(1002, "数据不存在"),

    USER_NOT_EXIST(2002, "用户不存在"),
    USERNAME_IN_USE(2003, "用户名已被使用"),
    LOGIN_NOT_MATCH(2004, "账号与密码不匹配"),
    PASSWORD_NOT_CHANGED(2005, "新密码与原密码重复"),
    VERIFY_CODE_WRONG(2006, "验证码输入错误"),
    VERIFY_CODE_NOT_SEND(2007, "验证码未发送或已过期"),

    MAIL_FAIL_TO_SEND(7009, "邮件发送错误"),

    UNKNOWN_ERROR_OCCURED(9999, "发生未知错误");


    private final Integer code;
    private final String message;

    ResultExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
