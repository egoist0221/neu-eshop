package com.neutech.mapper;

import com.neutech.entity.Shipping;
import com.neutech.form.ShippingForm;
import com.neutech.vo.ShippingVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    ShippingVO selectShippingFormById(Integer id);

    List<ShippingVO> selectShippingFormByUserId(Integer userId);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);
}