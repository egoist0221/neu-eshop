package com.neutech.mapper;

import com.neutech.entity.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {

    List<Product> listProduct();

    Product getOneById(Integer id);

    int save(Product product);

    int update(Product product);

    int deleteAllByIds(Integer[] ids);

}
