package com.neutech.mapper;

import com.neutech.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {

    Category getOneById(Integer id);

    List<Category> listByParentId(Integer parentId);

    List<Category> listTopStruct();

}
