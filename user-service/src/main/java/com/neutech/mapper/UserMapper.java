package com.neutech.mapper;

import com.neutech.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    Integer checkPassword(@Param("userId") Integer userId,
                          @Param("password") String password);

    Integer deleteByPrimaryKey(Integer id);

    Integer insertSelective(User record);

    Integer isUsernameInUse(String username);

    User selectByPrimaryKey(Integer id);

    User selectByUsername(String username);

    User login(@Param("username") String username,
               @Param("password") String password);

    Integer updateByPrimaryKeySelective(User record);

    Integer updatePassword(@Param("userId") Integer userId,
                           @Param("oldPassword") String oldPassword,
                           @Param("newPassword") String newPassword);
}