package com.neutech.controller;

import com.neutech.entity.User;
import com.neutech.service.UserService;
import com.neutech.vo.ResultVO;
import com.neutech.form.UserForm;
import com.neutech.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
public class UserController {

    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping("/login")
    public ResultVO<UserVO> login(@RequestParam("username") String username,
                                @RequestParam("password") String password) {
        return service.login(username, password);
    }

    /*
    @RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("email") String email,
                           @RequestParam(value = "phone", defaultValue = ""
     */
    @PostMapping("/regist")
    public ResultVO<Void> regist(@RequestBody @Valid UserForm userForm) {
        return service.regist(userForm);
    }

    @PostMapping("/checkUsername")
    public ResultVO<Void> checkUsername(@RequestParam("username") String username) {
        return service.checkUsername(username);
    }

    @PostMapping("/changeUserInfo")
    public ResultVO<UserVO> changeUserInfo(@RequestBody UserForm userForm) {
        return service.changeUserInfo(userForm);
    }

    @PostMapping("/checkPassword")
    public ResultVO<Void> checkPassword(@RequestParam("userId") Integer userId,
                                         @RequestParam("password") String password) {
        return service.checkPassword(userId, password);
    }

    @PostMapping("/changePassword")
    public ResultVO<Void> changePassword(@RequestParam("userId") Integer userId,
                                         @RequestParam("oldPassword") String oldPassword,
                                         @RequestParam("newPassword") String newPassword) {
        return service.changePassword(userId, oldPassword, newPassword);
    }

    @GetMapping("/sendVerifyMail")
    public ResultVO<Void> sendCodeEmail(@RequestParam("email") String receiver) {
        return service.sendVerifyMail(receiver);
    }

}
