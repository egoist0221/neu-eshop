package com.neutech.controller;

import com.neutech.form.ShippingForm;
import com.neutech.service.ShippingService;
import com.neutech.vo.ResultVO;
import com.neutech.vo.ShippingVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class ShippingController {

    private final ShippingService service;

    @Autowired
    public ShippingController(ShippingService service) {
        this.service = service;
    }

    @PostMapping("/addShipping")
    public ResultVO<Void> AddShipping(@RequestBody @Valid ShippingForm shippingForm){
        return service.addShipping(shippingForm);
    }

    @PostMapping("/deleteShipping")
    public ResultVO<Void> deleteShipping(@RequestParam("shippingId") Integer shippingId){
        return service.deleteShipping(shippingId);
    }

    @GetMapping("/getAllShipping")
    public ResultVO<List<ShippingVO>> getAllShipping(@RequestParam("userId") Integer userId){
        return service.getAllShipping(userId);
    }

    @GetMapping("/getShipping")
    public ResultVO<ShippingVO> getShipping(@RequestParam("shippingId") Integer shippingId){
        return service.getShipping(shippingId);
    }

    @PostMapping("/updateShipping")
    public ResultVO<Void> updateShipping(@RequestBody ShippingForm shippingForm){
        return service.updateShipping(shippingForm);
    }
}
