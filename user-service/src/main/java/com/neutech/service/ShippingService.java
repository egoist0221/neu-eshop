package com.neutech.service;

import com.neutech.form.ShippingForm;
import com.neutech.vo.ResultVO;
import com.neutech.vo.ShippingVO;

import java.util.List;

public interface ShippingService {
    ResultVO<Void> addShipping(ShippingForm sForm);
    ResultVO<Void> deleteShipping(Integer shippingId);
    ResultVO<ShippingVO> getShipping(Integer shippingId);
    ResultVO<List<ShippingVO>> getAllShipping(Integer userId);
    ResultVO<Void> updateShipping(ShippingForm sForm);
}
