package com.neutech.service.impl;

import com.neutech.mapper.CategoryMapper;
import com.neutech.service.CategoryService;
import com.neutech.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryServiceImpl(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    @Override
    public ResultVO listTopStruct() {
        return ResultVO.success(categoryMapper.listTopStruct());
    }
}
