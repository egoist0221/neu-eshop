package com.neutech.service.impl;

import com.neutech.entity.Shipping;
import com.neutech.enumeration.ResultExceptionEnum;
import com.neutech.form.ShippingForm;
import com.neutech.mapper.ShippingMapper;
import com.neutech.service.ShippingService;
import com.neutech.vo.ResultVO;
import com.neutech.vo.ShippingVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ShippingServiceImpl implements ShippingService {

    private final ShippingMapper mapper;

    @Autowired
    public ShippingServiceImpl(ShippingMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ResultVO<Void> addShipping(ShippingForm sForm) {
        Shipping shipping = parseFormToShipping(sForm);
        shipping.setCreateTime(new Date());
        int code = mapper.insert(shipping);
        if (code != 1)
            return ResultVO.error(ResultExceptionEnum.UNKNOWN_ERROR_OCCURED);
        return ResultVO.success();
    }

    @Override
    public ResultVO<Void> deleteShipping(Integer shippingId) {
        int code = mapper.deleteByPrimaryKey(shippingId);
        if (code != 1)
            return ResultVO.error(ResultExceptionEnum.UNKNOWN_ERROR_OCCURED);
        return ResultVO.success();
    }

    @Override
    public ResultVO<ShippingVO> getShipping(Integer shippingId) {
        ShippingVO shippingVO = mapper.selectShippingFormById(shippingId);
        if (shippingVO == null)
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);
        return ResultVO.success(shippingVO);
    }

    @Override
    public ResultVO<List<ShippingVO>> getAllShipping(Integer userId) {
        List<ShippingVO> shippingVOList = mapper.selectShippingFormByUserId(userId);
        if (shippingVOList == null || shippingVOList.isEmpty())
            return ResultVO.error(ResultExceptionEnum.DATA_NON_EXISTS);
        return ResultVO.success(shippingVOList);
    }

    @Override
    public ResultVO<Void> updateShipping(ShippingForm sForm) {
        Shipping shipping = parseFormToShipping(sForm);
        shipping.setUpdateTime(new Date());
        int code = mapper.updateByPrimaryKeySelective(shipping);
        if (code != 1)
            return ResultVO.error(ResultExceptionEnum.UNKNOWN_ERROR_OCCURED);
        return ResultVO.success();
    }

    private Shipping parseFormToShipping(ShippingForm shippingForm) {
        Shipping shipping = new Shipping();
        shipping.setId(shippingForm.getId());
        shipping.setUserId(shippingForm.getUserId());
        shipping.setReceiverName(shippingForm.getReceiver());
        shipping.setReceiverPhone(shippingForm.getPhone());
        shipping.setReceiverProvince(shippingForm.getProvince());
        shipping.setReceiverCity(shippingForm.getCity());
        shipping.setReceiverDistrict(shippingForm.getDistrict());
        shipping.setReceiverAddress(shippingForm.getAddress());
        shipping.setReceiverZip(shippingForm.getZip());
        return shipping;
    }
}
