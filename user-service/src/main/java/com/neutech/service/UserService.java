package com.neutech.service;

import com.neutech.form.UserForm;
import com.neutech.vo.ResultVO;
import com.neutech.vo.UserVO;

public interface UserService {
    ResultVO<Void> regist(UserForm userForm);

    ResultVO<UserVO> login(String username, String password);

    ResultVO<Void> checkUsername(String username);

    ResultVO<UserVO> changeUserInfo(UserForm userForm);

    ResultVO<Void> sendVerifyMail(String receiver);

    ResultVO<Void> checkPassword(Integer userId, String password);

    ResultVO<Void> changePassword(Integer userId, String oldPassword, String newPassword);
}
