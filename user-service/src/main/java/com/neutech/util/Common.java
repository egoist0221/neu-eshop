package com.neutech.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Common {
    public static String getTodayString(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ss");
        return sdf.format(new Date());
    }
}
